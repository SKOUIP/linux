# TP Linux 2

## O. Prérequis
* Configuration réseau serveur base de données
```bash=
$ sudo vim /etc/sysconfig/network-scripts/ens37
    DEVICE=ens37
    IPADDR=192.168.177.11
    GATEWAY=255.255.255.0
    ONBOOT=yes
    BOOTPROTO=static
    
$ sudo vim /etc/sysconfig/network
    HOSTNAME=db.tp2.cesi
    DNS1=1.1.1.1

$ sudo systemctl restart network

ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:a0:8a:a0 brd ff:ff:ff:ff:ff:ff
    inet 192.168.128.129/24 brd 192.168.128.255 scope global noprefixroute dynamic ens33
       valid_lft 1734sec preferred_lft 1734sec
    inet6 fe80::978d:a320:1108:fc3d/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: ens37: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:a0:8a:aa brd ff:ff:ff:ff:ff:ff
    inet 192.168.177.11/24 brd 192.168.177.255 scope global noprefixroute ens37
       valid_lft forever preferred_lft forever
    inet6 fe80::20c:29ff:fea0:8aaa/64 scope link
       valid_lft forever preferred_lft forever
    
$ sudo vim /etc/hosts
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6     localhost6.localdomain6
    192.168.177.11 db.tp2 db.tp2.cesi
    
$ sudo vim /etc/hostname
    HOSTNAME=db.tp2.cesi
``` 
* Configuration réseau serveur WEB
```bash=
$ sudo vim /etc/sysconfig/network-scripts/ens37
    DEVICE=ens37
    IPADDR=192.168.177.10
    GATEWAY=255.255.255.0
    ONBOOT=yes
    BOOTPROTO=static

$ sudo vim /etc/sysconfig/network
    HOSTNAME=web.tp2.cesi
    DNS1=1.1.1.1

$ sudo systemctl restart network

4 ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:2c:e0:bc brd ff:ff:ff:ff:ff:ff
    inet 192.168.128.130/24 brd 192.168.128.255 scope global noprefixroute dynamic ens33
       valid_lft 1774sec preferred_lft 1774sec
    inet6 fe80::dcf9:9f5d:f337:2ee0/64 scope link tentative noprefixroute dadfailed
       valid_lft forever preferred_lft forever
    inet6 fe80::978d:a320:1108:fc3d/64 scope link tentative noprefixroute dadfailed
       valid_lft forever preferred_lft forever
    inet6 fe80::ca9f:e287:4a01:c9ea/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: ens37: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:2c:e0:c6 brd ff:ff:ff:ff:ff:ff
    inet 192.168.177.10/24 brd 192.168.177.255 scope global noprefixroute ens37
       valid_lft forever preferred_lft forever
    inet6 fe80::20c:29ff:fe2c:e0c6/64 scope link
       valid_lft forever preferred_lft forever
    
$ sudo vim /etc/hosts
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6     localhost6.localdomain6
    192.168.177.10 web.tp2 web.tp2.cesi

$ sudo vim /etc/hostname
    HOSTNAME=web.tp2.cesi
``` 
* Configuration réseau serveur Reverse Proxy
```bash=

$ sudo vim /etc/sysconfig/network-scripts/ens37
    DEVICE=ens37
    IPADDR=192.168.177.12
    GATEWAY=255.255.255.0
    ONBOOT=yes
    BOOTPROTO=static
    
$ sudo vim /etc/sysconfig/network
    HOSTNAME=rp.tp2.cesi
    DNS1=1.1.1.1

$ sudo systemctl restart network

ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
    2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:cb:02:af brd ff:ff:ff:ff:ff:ff
    inet 192.168.128.128/24 brd 192.168.128.255 scope global noprefixroute dynamic ens33
       valid_lft 1764sec preferred_lft 1764sec
    inet6 fe80::978d:a320:1108:fc3d/64 scope link tentative noprefixroute dadfailed
       valid_lft forever preferred_lft forever
    inet6 fe80::dcf9:9f5d:f337:2ee0/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
    3: ens37: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:cb:02:b9 brd ff:ff:ff:ff:ff:ff
    inet 192.168.177.12/24 brd 192.168.177.255 scope global noprefixroute ens37
       valid_lft forever preferred_lft forever
    inet6 fe80::20c:29ff:fecb:2b9/64 scope link
       valid_lft forever preferred_lft forever

$ sudo vim /etc/hosts
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6     localhost6.localdomain6
    192.168.177.12 rp.tp2 rp.tp2.cesi

$ sudo vim /etc/hostname
    HOSTNAME=rp.tp2.cesi
```
## I. Base de données

* Installation MariaDB
```bash=
$ sudo yum install mariadb-server

$ sudo yum list mariadb-server
    Loaded plugins: fastestmirror
    Determining fastest mirrors
     * base: miroir.univ-paris13.fr
     * extras: miroir.univ-paris13.fr
     * updates: miroir.univ-paris13.fr
    Installed Packages
    mariadb-server.x86_64         1:5.5.68-1.el7            @base
    
$ sudo systemctl enable mariadb --now 
    Created symlink from /etc/systemd/system/multi-user.target.wants/mariadb.service to /usr/lib/systemd/system/mariadb.service.
    
$ sudo mysql_secure_installation
    Set root password? [Y/n] y
    New password:
    Re-enter new password:
    Password updated successfully!
    Reloading privilege tables..
     ... Success!


    By default, a MariaDB installation has an anonymous user, allowing     anyone
    to log into MariaDB without having to have a user account created for
    them.  This is intended only for testing, and to make the installation
    go a bit smoother.  You should remove them before moving into a
    production environment.

    Remove anonymous users? [Y/n] y
     ... Success!

    Normally, root should only be allowed to connect from localhost.   This
    ensures that someone cannot guess at the root password from the network.

    Disallow root login remotely? [Y/n] y
     ... Success!

    By default, MariaDB comes with a database named test that anyone can
    access.  This is also intended only for testing, and should be removed
    before moving into a production environment.

    Remove test database and access to it? [Y/n] y
     - Dropping test database...
     ... Success!
     - Removing privileges on test database...
     ... Success!

    Reloading the privilege tables will ensure that all changes made so far
    will take effect immediately.

    Reload privilege tables now? [Y/n] y
     ... Success!

    Cleaning up...

    All done !  If you have completed all of the above steps, your MariaDB
    installation should now be secure.

    Thanks for using MariaDB!
``` 
* Configuration base de données
```bash=
$ mysql -u root -p

$ use mysql;

$ delete from user where host!='localhost';

$ create database webapp;
    Query OK, 1 row affected (0.00 sec)

$ show databases;
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    | webapp             |
    +--------------------

$ grant all on webapp.* to webappuser@192.168.177.10
    -> identified by 'Formation2020';
    Query OK, 0 rows affected (0.00 sec)
    
$ select user, host, password from user;

+------------+----------------+-------------------------------------------+
| user       | host           | password                                  |
+------------+----------------+-------------------------------------------+
| root       | localhost      | *941104763BB45A3A58DFD76B3977ABB94E121D8A |
| webappuser | 192.168.177.10 | *941104763BB45A3A58DFD76B3977ABB94E121D8A 
+------------+----------------+-------------------------------------------+

$ sudo ss -alnpt 
    State      Recv-Q Send-Q               Local Address:Port                              Peer Address:Port
    LISTEN     0      50                               *:3306                                         *:*                   users:    (("mysqld",pid=1745,fd=14))
    LISTEN     0      128                              *:22                                           *:*                   users:    (("sshd",pid=1135,fd=3))
    LISTEN     0      5                                *:8888                                         *:*                   users:    (("python2",pid=736,fd=3))
    LISTEN     0      128                           [::]:22                                        [::]:*                   users:    (("sshd",pid=1135,fd=4))

$ sudo firewall-cmd --add-port=3306/tcp --permanent
    success

$ sudo firewall-cmd --reload
    success

$ sudo firewall-cmd --list-ports
    888/tcp 3306/tcp
```

## II. Serveur Web
* Installation des services HTTPD, PHP et WGET
```bash=
$ sudo yum install httpd -y 

$ sudo yum list httpd 
    Loaded plugins: fastestmirror
    Loading mirror speeds from cached hostfile
     * base: centos.mirror.fr.planethoster.net
     * extras: centos.mirror.fr.planethoster.net
     * updates: centos.mirror.fr.planethoster.net
    Installed Packages
    httpd.x86_64              2.4.6-97.el7.centos          @updates

    
# Installation de dépôt additionels
$ sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y

$ sudo yum install -y yum-utils

# On supprime d'éventuelles vieilles versions de PHP précédemment installées
$ sudo yum remove -y php

# Activation du nouveau dépôt
$ sudo yum-config-manager --enable remi-php56  

# Installation de PHP 5.6.40 et de librairies récurrentes
$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y

$ sudo yum list php 
    Loaded plugins: fastestmirror
    Loading mirror speeds from cached hostfile
    ^[[A^[[A * base: centos.mirror.fr.planethoster.net
     * epel: mirrors.ircam.fr
     * extras: centos.mirror.fr.planethoster.net
     * remi-php56: rpms.remirepo.net
     * remi-safe: rpms.remirepo.net
     * updates: centos.mirror.fr.planethoster.net
    Installed Packages 
    php.x86_64                 5.6.40-23.el7.remi      @remi-php56
    
$ sudo  yum install wget -y 

$ sudo yum list wget
    Loaded plugins: fastestmirror
    Loading mirror speeds from cached hostfile
     * base: centos.mirror.fr.planethoster.net
     * extras: centos.mirror.fr.planethoster.net
     * updates: centos.mirror.fr.planethoster.net
    Installed Packages
    wget.x86_64                1.14-18.el7_6.1         @base
```
* Téléchargement Wordpress et décompression
```bash=
$ sudo wget http://wordpress.org/latest.tar.gz 
    2020-12-16 14:20:14 (573 KB/s) - ‘latest.tar.gz’ saved [15422346/15422346]

$ sudo tar xzvf latest.tar.gz
```
* Transfert dossier Wordpress vers /var/www/html
```bash=
$ sudo yum install rsync -y 

$ sudo yum list rsync
    Loaded plugins: fastestmirror
    Loading mirror speeds from cached hostfile

     * base: centos.mirror.fr.planethoster.net
     * epel: mirrors.ircam.fr
     * extras: centos.mirror.fr.planethoster.net
     * remi-php56: remirepo.reloumirrors.net
     * remi-safe: remirepo.reloumirrors.net
     * updates: centos.mirror.fr.planethoster.net
    Installed Packages
    rsync.x86_64                3.1.2-10.el7           @base

$ sudo rsync -avP ~/wordpress/ /var/www/html/

$ ls /var/www/html/
    index.php    wp-activate.php     wp-comments-post.php  wp-cron.php        wp-load.php   wp-settings.php   xmlrpc.php
    license.txt  wp-admin            wp-config-sample.php  wp-includes        wp-login.php  wp-signup.php
    readme.html  wp-blog-header.php  wp-content            wp-links-opml.php  wp-mail.php   wp-trackback.php
```
* Modification du fichier de config de Wordpress
```bash=
$ sudo cd /var/www/html  

$ sudo cp wp-config-sample.php wp-config.php

$ sudo vim wp-config.php

    // ** MySQL settings - You can get this info from your web host ** //
    /** The name of the database for WordPress */
    define( 'DB_NAME', 'webapp' );

    /** MySQL database username */
    define( 'DB_USER', 'webappuser' );

    /** MySQL database password */
    define( 'DB_PASSWORD', 'Formation2020' );

    /** MySQL hostname */
    define( 'DB_HOST', '192.168.177.11' );

    /** Database Charset to use in creating database tables. */
    define( 'DB_CHARSET', 'utf8' );

    /** The Database Collate type. Don't change this if in doubt. */
    define( 'DB_COLLATE', '' );'
```
* Démarrage du service HTTPD
```bash=
$ sudo systemctl start httpd.service
```
* Ouverture du port Apache via firewall-cmd
```bash=
$ sudo firewall-cmd --permanent --zone=public --add-service=http
    success
    
$ sudo firewall-cmd --permanent --zone=public --add-service=https   
    success

$ sudo firewall-cmd --reload
    success
```
* Activation du service HTTPD au démarrage de la machine 
```bash=
$ sudo systemctl enable httpd.service
    Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
## Accès à http://192.168.177.10 vérifier depuis navigateur internet de ma machine: Success! WordPress has been installed. Thank you, and enjoy!
```
## III. Reverse Proxy
* Installation du dépôt EPEL et du paquet NGINX
```bash=
$ sudo yum install epel-release -y

$ sudo yum list epel-release
    Loaded plugins: fastestmirror
    Loading mirror speeds from cached hostfile
    epel/x86_64/metalink                                                                             |  32 kB  00:00:00
     * base: centos.mirror.fr.planethoster.net
     * epel: mirrors.ircam.fr
     * extras: distrib-coffee.ipsl.jussieu.fr
     * updates: centos.mirror.fr.planethoster.net
    epel                                                                                             | 4.7 kB  00:00:00
    (1/3): epel/x86_64/group_gz                                                                      |  95 kB  00:00:00
    (2/3): epel/x86_64/updateinfo                                                                    | 1.0 MB  00:00:00
    (3/3): epel/x86_64/primary_db                                                                    | 6.9 MB  00:00:01
    Installed Packages
    epel-release.noarch               7-11                    @extras
    Available Packages
    epel-release.noarch               7-13                    epel

$ sudo yum install nginx -y
    Loaded plugins: fastestmirror
    Determining fastest mirrors
     * base: centos.quelquesmots.fr
     * epel: mirrors.ircam.fr
     * extras: centos.quelquesmots.fr
     * updates: centos.quelquesmots.fr
    Installed Packages
    nginx.x86_64                       1:1.16.1-3.el7         @epel
```
* Démarrage du service NGINX 
```bash=
$ sudo systemctl start nginx && systemctl enable nginx
```
* Autorisation du protocole HTTP dans le pare-feu
```bash=
$ sudo firewall-cmd --permanent --zone=public --add-service=http

$ sudo firewall-cmd --permanent --zone=public --add-service=https

$ sudo firewall-cmd --reload
```
* Modification du fichier de config de NGINX 
```bash=
$ sudo vim /etc/nginx/nginx.conf

    events {}

    http {
       server {
            listen       80;

            server_name web.cesi;

            location / {
                proxy_pass   http://192.168.177.10;
            }
        }
    }
```
* Vérification de la cohérence du fichier de config de NGINX
```bash=
$ sudo nginx -t -c /etc/nginx/nginx.conf
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```
* Rechargement de NGINX

```bash=
sudo systemctl reload nginx
```
## IV. Un peu de sécu
### 1-Fail2ban
* Installation des paquets fail2ban-server et fail2ban-firewalld
```bash=
$ sudo yum install fail2ban-server fail2ban-firewalld

$ sudo yum list fail2ban 
    Loaded plugins: fastestmirror
    Loading mirror speeds from cached hostfile
     * base: centos.mirror.fr.planethoster.net
     * epel: mirrors.ircam.fr
     * extras: distrib-coffee.ipsl.jussieu.fr
     * updates: centos.mirror.fr.planethoster.net
    Installed Packages
    fail2ban.noarch             0.11.1-10.el7                      @epel
```
* Cration et configuration du fichier de config ssh.local
```bash=
$ sudo vim /etc/fail2ban/jail.d/sshd.local
    [DEFAULT]
    bantime = 3600

    [sshd]
    enabled = true
```
* Démarrage du service fail2ban
```bash=
$ sudo systemctl enable fail2ban --now

$ sudo fail2ban-client status
    Status
    |- Number of jail:      1
    `- Jail list:   sshd
    
$ sudo tail -f /var/log/fail2ban.log

    2020-12-17 10:27:43,942 fail2ban.jail           [1950]: INFO    Creating new jail 'sshd'
    2020-12-17 10:27:43,955 fail2ban.jail           [1950]: INFO    Jail 'sshd' uses systemd {}
    2020-12-17 10:27:43,955 fail2ban.jail           [1950]: INFO    Initiated 'systemd' backend
    2020-12-17 10:27:43,956 fail2ban.filter         [1950]: INFO      maxLines: 1
    2020-12-17 10:27:43,956 fail2ban.filtersystemd  [1950]: INFO    [sshd] Added journal match for: '_SYSTEMD_UNIT=sshd.service + _COMM=sshd'
    2020-12-17 10:27:43,976 fail2ban.filter         [1950]: INFO      maxRetry: 5
    2020-12-17 10:27:43,976 fail2ban.filter         [1950]: INFO      encoding: UTF-8
    2020-12-17 10:27:43,976 fail2ban.filter         [1950]: INFO      findtime: 600
    2020-12-17 10:27:43,977 fail2ban.actions        [1950]: INFO      banTime: 86400
    2020-12-17 10:27:43,978 fail2ban.jail           [1950]: INFO    Jail 'sshd' started
```
### 2-HTTPS
* Génération du certificat et de la clé privée associée
```bash=
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.cesi.crt
Generating a 2048 bit RSA private key
    ........................................................+++
    ...................+++
    writing new private key to 'web.cesi.key'
    -----
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) [XX]:FR
    State or Province Name (full name) []:Gironde
    Locality Name (eg, city) [Default City]:Bordeaux
    Organization Name (eg, company) [Default Company Ltd]:CESI
    Organizational Unit Name (eg, section) []:cesi
    Common Name (eg, your name or your server's hostname) []:web.cesi
    Email Address []:'
```
* Transfert de la clé et du certificat vers les répertoires appropriés
```bash=
sudo mv /home/tanguy/web.cesi.crt /etc/pki/tls/certs/

ls /etc/pki/tls/certs/
    ca-bundle.crt  ca-bundle.trust.crt  make-dummy-cert  Makefile  renew-dummy-cert  web.cesi.crt

sudo mv /home/tanguy/web.cesi.key /etc/pki/tls/private/

ls /etc/pki/tls/private 
    web.cesi.key
```
* Configuration Reverse Proxy (écoute sur le port 443)
```bash=
sudo vim /etc/nginx/nginx.conf*
    events {}

    http {
       server {

            listen      443;
            server_name web.cesi;
            ssl on;
            ssl_certificate "/etc/pki/tls/certs/web.cesi.crt";
            ssl_certificate_key     "/etc/pki/tls/private/web.cesi.key";
            location / {
                proxy_pass   http://192.168.177.10;
            }
        }
       server{

            listen 80 default_server;
            server_name web.cesi;
            return 301 https://$host$request_uri;
             }
    }

sudo nginx -t -c /etc/nginx/nginx.conf
    nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
    nginx: configuration file /etc/nginx/nginx.conf test is successful

sudo systemctl restart nginx

ss -alpnt 
    State      Recv-Q Send-Q               Local Address:Port                              Peer Address:Port
    LISTEN     0      128                              *:80                                           *:*
    LISTEN     0      128                              *:22                                           *:*
    LISTEN     0      5                                *:8888                                         *:*
    LISTEN     0      128                              *:443                                          *:*
    LISTEN     0      128                           [::]:22                                        [::]:*