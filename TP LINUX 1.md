# TP LINUX 1


## Creation user
```bash=
$root adduser gabin
$root sudo groupadd admin
$root sudo visudo
$root sudo usermod -aG admin gabin

ps auxwww |grep sshd: # afficher les users

# elever les droits du groupe 
$root sudo visudo
    ##############################
    ## Allows people in group wheel to run all commands
    %weel   ALL=(ALL)       ALL
    %admin  ALL=(ALL)       ALL
    ##############################
    
$root sudo cat /etc/sudoers #verifier les modifications

#installation de vim
$ sudo apt-get install vim
$ sudo yum install vim

```
## Configuration reseau
```bash=
#modification du hotname
$ sudo vim /etc/hostname
$ cat /etc/hostname #verifier le hostname

#modification de l'IP
$ sudo vim /etc/resolv.conf
$ cat /etc/resolv.conf #verifier l'ip

$ip a #check de l'interface
    #######
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
           valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
           valid_lft forever preferred_lft forever
    2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
        link/ether 00:0c:29:31:cd:e7 brd ff:ff:ff:ff:ff:ff
        inet 192.168.17.128/24 brd 192.168.17.255 scope global noprefixroute dynamic ens33
           valid_lft 1589sec preferred_lft 1589sec
        inet6 fe80::5814:c086:b33:2e77/64 scope link noprefixroute
           valid_lft forever preferred_lft forever
    ######

$ sudo vi /etc/sysconfig/network-scripts/ifcfg-ens33:
    #######
    NAME=ens33:
    DEVICE=ens33:

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=192.168.17.128
    NETMASK=255.255.255.0

    GATEWAY=192.168.1.254
    DNS1=1.1.1.1
    ###### modification de l'interface
$ ping google.com #check de la connection (peut différer en fonction de la carte reseau)

#configurer le SSh
#creer la clé
 ssh-keygen -t RSA -b 4096 #sur le Windows hote
 
#donner la clé a linux 
$ vim /home/gabin/.ssh
    #####
    $cle # y copier la clé .
    #####

#commexion : ssh gabin@192.168.17.128 (sur windows)
    
  ```
  ## Particionnement
  ```bash=    

lsblk #lister les disques
    #####
    NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
    sda               8:0    0   20G  0 disk
    ├─sda1            8:1    0    1G  0 part /boot
    └─sda2            8:2    0   19G  0 part
      ├─centos-root 253:0    0   17G  0 lvm  /
      └─centos-swap 253:1    0    2G  0 lvm  [SWAP]
    sdb               8:16   0    3G  0 disk
    sdc               8:32   0    3G  0 disk
    sr0              11:0    1  973M  0 rom
    #####

#monter les lisques
$ sudo pvcreate /dev/sdb
$ sudo pvcreate /dev/sdc
$ sudo pvcreate /dev/sdd

    # Vérif
    $ sudo vgs
    $ sudo vgdisplay

#creer un volume
$ sudo vgcreate data /dev/sdb
$ sudo vgcreate data2 /dev/sdd
#etendre le volume a un autre disque
$ sudo vgextend data /dev/sdc
    # Vérif
    $ sudo vgs
    $ sudo vgdisplay

#creation d'un volume logique
$ sudo lvcreate -l 100%FREE data -n big_data
$ sudo lvcreate -l 100%FREE data2 -n small_data
    #verif
    $ sudo vgs

#formatage
$ sudo mkfs -t ext4 /dev/data/big_data
$ sudo mkfs -t ext4 /dev/data2/small_data
#creation de dossier pour faire le lien
$ sudo mkdir /mnt/data1
$ sudo mkdir /mnt/data2
#creation des point de montage
$ sudo mount /dev/data/big_data /mnt/data1
$ sudo mount /dev/data/big_data /mnt/data1
    # Vérif 
    $ mount
    $ df -h

#faire monter les lecteurs au boot
$ sudo vim /etc/fstab
    #########
    /dev/data/big_data /mnt/data1 ext4 defaults 0 0
    /dev/data2/small_data  /mnt/data2 ext4 defaults 0 0
    #########
```
## Gestion des services
```bash=
#optenir des info sur 'firewalld'
systemctl status firewalld
systemctl is-active firewalld

#creer un service
$ sudo vim /etc/systemd/system/web.service
    ###################
    [Unit]
    Description=Very simple web service

    [Service]
    ExecStart=/bin/python2 -m SimpleHTTPServer 8888

    [Install]
    WantedBy=multi-user.target
    ############################
    
#ouvrir le port 8888
$ sudo firewall-cmd --add-port=8888/tcp --permanent

    #verif
    systemctl status

#reinitialiser les demon et faire demarrer le service
$sudo systemctl daemon-reload
$sudo systemctl enable web.service
$sudo systemctl start web.service
    #verif
    curl 192.168.17.128:8888 |more

#Attribuer le service au user dedié (web)
$ sudo adduser web
$ sudo mkdir /srv/web_folder
$ sudo vim /etc/systemd/system/web.service
    #############################
	[Unit]
	Description=Very simple web service

	[Service]
	ExecStart=/bin/python2 -m SimpleHTTPServer 8888
	User=web
	WorkingDirectory=/srv/web_folder

    [Install]
    WantedBy=multi-user.target
    ############################
